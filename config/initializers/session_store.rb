# Be sure to restart your server when you modify this file.

# Your secret key for verifying cookie session data integrity.
# If you change this key, all old sessions will become invalid!
# Make sure the secret is at least 30 characters and all random, 
# no regular words or you'll be exposed to dictionary attacks.
ActionController::Base.session = {
  :key         => '_sushi_session',
  :secret      => '6c7837f732a375dc65539cf2cfe4c075c8def292e1e0d09f93a5129fb26900981a4300b95f4be3c74af4118f2d0fd22aa1f7d8e621ab4d402f0233d80167c103'
}

# Use the database for sessions instead of the cookie-based default,
# which shouldn't be used to store highly confidential information
# (create the session table with "rake db:sessions:create")
# ActionController::Base.session_store = :active_record_store
